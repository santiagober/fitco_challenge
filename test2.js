// --- Directions
// Given a string, return the character that is most
// commonly used in the string.
// --- Examples
// maxChar("abcccccccd") === "c"
// maxChar("apple 1231111") === "1"

function test2(str) {
  const obj = str.split('').reduce(function (acc, curr) { // get letters with quantity
    return {
      ...acc,
      [curr]: acc[curr] ? acc[curr] + 1 : 1
    }
  }, {})

  let val = 0, letter = ''
  for (var k in obj)
    if (obj[k] >= val) {
      obj[k] === val ? letter = letter + ',' + k : letter = k
      val = obj[k]
    }

  return letter
}

module.exports = test2;
