/*falta tabla culqi*/
SET @DateInit = '2010-01-01'; SET @DateEnd = '2020-05-31';
SELECT p.id planId, p.name planName, m.id membershipId, rs.abrev, rs.total FROM (
select ss.id, ss.membershipId, IF(ss.paid = 0, 'Deu', 'Ing') abrev, SUM(ss.price) total
from subscription_stripe_listPaid ss
where ss.disDate IS NULL AND
((ss.paid = 0 AND ss.endDatePaid < current_date() AND ss.endDatePaid BETWEEN @DateInit AND @DateEnd) OR 
(ss.paid = 1 AND ss.updDate BETWEEN @DateInit AND @DateEnd))
/*ss.updDate between @DateInit AND @DateEnd AND
(ss.paid = 1 OR (ss.endDatePaid < current_date() AND ss.paid = 0))*/
group by ss.membershipId, abrev
order by ss.id,ss.membershipId) rs 
LEFT JOIN memberships m ON rs.membershipId = m.id
LEFT JOIN plans p ON p.id = m.planId
WHERE m.disDate IS NULL 
AND m.typePayment = 1 AND m.status = 1
