import { Injectable } from '@angular/core';
import * as io from 'socket.io-client'
import { Observable } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class WsService {
  socket: any;

  constructor() {
    this.socket = io('http://localhost:5050/')
  }


  listener(event: String) {
    return new Observable((subscriber) => {
      this.socket.on(event, (data: any) => {
        subscriber.next(data)
      })
    })
  }

  emitterPayload(event: String, product: any) {
    this.socket.emit(event, product);
  }

  emitter(event: String) {
    this.socket.emit(event);
  }
}
