import { Component, OnInit } from '@angular/core';
import { WsService } from './../../serv/ws.service'

// export interface Section {
//   name: string;
//   updated: Date;
// }

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})

export class ProductsComponent implements OnInit {
  product = {
    name: ''
  }
  listProducts: any = [];

  constructor(private ws: WsService) { }

  ngOnInit(): void {
    this.ws.listener('upd-list-server').subscribe((data) => {
      console.log(data)
      this.listProducts = data;
    })
    this.ws.emitter('getList')
  }

  sendProduct() {
    this.ws.emitterPayload('send-product-client', this.product)
    this.product.name = ''
  }

}
