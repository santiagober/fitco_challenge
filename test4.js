// --- Directions

//Alice is a kindergarten teacher. She wants to give some candies to the children in her class.  
//All the children sit in a line and each of them has a rating score according to his or her performance in the class. 
//Alice wants to give at least 1 candy to each child. If two children sit next to each other, then the one with the higher rating must get more candies. 
//Alice wants to minimize the total number of candies she must buy.
//For example, assume her students' ratings are [4, 6, 4, 5, 6, 2]. She gives the students candy in the following minimal amounts: [1, 2, 1, 2, 3, 1]. 
//She must buy a minimum of 10 candies


function test4(arr) {
  const resume = arr.slice(0).reduce(function (acc, curr) {
    const cand = !acc.prevScore || curr > acc.prevScore ? acc.prevCand + 1 : curr < acc.prevScore ? 1 : acc.prevCand // calculate amount of candy
    return {
      prevScore: curr,
      total: acc.total + cand,
      prevCand: cand
    }
  }, { prevScore: 0, prevCand: 0, total: 0 })

  return resume.total
}

module.exports = tes4;


