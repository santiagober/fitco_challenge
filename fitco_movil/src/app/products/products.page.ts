import { Component, OnInit } from '@angular/core';
import { Socket } from 'ng-socket-io'
import { Observable } from 'rxjs'

@Component({
  selector: 'app-products',
  templateUrl: './products.page.html',
  styleUrls: ['./products.page.scss'],
})
export class ProductsPage implements OnInit {
  product = {
    name: ''
  }
  listProducts: any = [];


  constructor(private socket: Socket) {
    // this.getProducts().subscribe(data => this.listProducts = data)
    this.getProducts().subscribe(data => {
      console.log(data)
      this.listProducts = data
    })
    this.reload()
  }

  sendProduct() {
    this.socket.emit('send-product-client', { name: this.product.name })
    this.product.name = ''
  }

  reload() {
    this.socket.emit('getList')
    this.product.name = ''
  }

  getProducts() {
    return new Observable(subscriber => {
      this.socket.on('upd-list-server', data => {
        subscriber.next(data)
      })
    })
  }

  ngOnInit() {
  }

}
