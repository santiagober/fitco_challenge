const app = require('express')()
const morgan = require('morgan')
const server = require('http').Server(app)
const io = require('socket.io')(server)

let list = []

// const app = express()
// const router = express.Router()

app.use(morgan('dev'))

// app.use(express.json())
// app.use(express.urlencoded({ extended: true }))
// app.use((req, res, next) => {
//   console.log(req.headers)
//   next()
// })
// app.listen(process.env.PORT, () => {
//   console.log('app listening in port: ' + process.env.PORT)
// })

io.on('connection', function (socket) {
  socket.on('send-product-client', function (product) {
    list.push(product)
    socket.emit('upd-list-server', list)
    socket.broadcast.emit('upd-list-server', list)
  })

  socket.on('getList', function (product) {
    socket.emit('upd-list-server', list)
    socket.broadcast.emit('upd-list-server', list)
  })
})

server.listen(process.env.PORT, function () {
  console.log('app listening in port: ' + process.env.PORT)
})

// router.use('/api/v1', v1)

// app.use('/', router)
